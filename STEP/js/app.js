const tabs = document.body.querySelector('.tabs');
	const tabsContent = document.body.querySelector('.tabs-content');

	function handlerEventListener(event) {
		tabs.querySelector('.active').classList.remove('active');
		event.target.classList.add('active');
		tabsContent.querySelector('li:not([hidden])').hidden = true;
		tabsContent.children[event.target.dataset.index].hidden = false;
	};

	for (let i = 0; i < tabs.childElementCount; i++) {
		tabs.children[i].dataset.index = i;
		if (i) {
			tabsContent.children[i].hidden = true;
		}
	};

	tabs.addEventListener('click', handlerEventListener);

	$('.slider-for').slick({

		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.slider-nav'
	});
	
	$('.slider-nav').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		centerPadding: 0,
		focusOnSelect: true,
		arrows: true,
		centerMode: true,
		asNavFor: '.slider-for'
	});
	
	$('.slider-for').on('afterChange', function (event, slick, currentSlide, nextSlide) {
		$('.testimonial-text').removeClass('activity').eq(currentSlide).addClass('activity');
		$('.testimonial-name-person').removeClass('activity').eq(currentSlide).addClass('activity');
		$('.testimonial-person-profession').removeClass('activity').eq(currentSlide).addClass('activity');
		$('.person-image-slider-nav').eq(nextSlide).addClass('activity');
	});
	