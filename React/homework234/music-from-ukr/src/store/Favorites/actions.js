import { SET_FAVORITES } from './types';

export const saveFavoritesAction = favorites => ({ type: SET_FAVORITES, payload: favorites });
