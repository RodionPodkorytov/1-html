import { SET_CART } from './types';

export const saveCartAction = cart => ({ type: SET_CART, payload: cart });
