import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { saveCartAction } from '../../../store/Cart/actions';
import { getCartSelector } from '../../../store/Cart/selectors';
import { saveLocal } from '../../../utils/saveLocal';
import { buttonStyle } from '../../../utils/style';
import Button from '../../Button/Button';
import Modal from '../Modal/Modal';

const AddCartModal = ({ ButtonHandler, product }) => {
  const productsCart = useSelector(getCartSelector);
  const dispatch = useDispatch();
  console.log(productsCart);
  const AddToCart = () => {
    let newCart = [];
    if (productsCart.length === 0) {
      newCart = [product];
    } else if (productsCart.includes(product)) {
      newCart = [...productsCart];
    } else {
      newCart = [...productsCart, product];
    }

    dispatch(saveCartAction(newCart));
    saveLocal(newCart, 'cart');
    ButtonHandler();
  };

  return (
    <Modal
      header={`ADD TO CART?`}
      text={`Do you really want to add ${product.artist} - ${product.album} to cart?`}
      actions={
        <>
          <Button styles={buttonStyle} text='Ok' onClick={AddToCart} />
          <Button styles={buttonStyle} text='Cancel' onClick={ButtonHandler} />
        </>
      }
      buttonHandler={ButtonHandler}
    />
  );
};

export default AddCartModal;
