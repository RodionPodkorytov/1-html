import React, { useRef } from "react";
import PropTypes from "prop-types";
import "./Modal.scss";

const Modal = ({ header, closeButton, text, actions, buttonHandler, onClick }) => {
  const modalRef = useRef(null);
  const closeRef = useRef(null);

  const closeBtnHandler = (e) => {
    const modal = modalRef.current;
    const close = closeRef.current;
    if (modal.contains(e.target) && e.target !== close) {
      return;
    }
    buttonHandler();
  };

  return (
    <div onClick={closeBtnHandler} className="modal-fade">
      <div className="modal" ref={modalRef}>
        <div className="modal__header">
          <h2 className="modal__title">{header}</h2>
          {closeButton && (
            <span className="modal__close" ref={closeRef}>
              &times;
            </span>
          )}
        </div>
        <div className="modal__body">
          <p>{text}</p>
        </div>
        <div className="modal__footer">{actions}</div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.object,
};

export default Modal;
