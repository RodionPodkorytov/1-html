import React, { useState, useEffect } from "react";
import PropTypes, { array } from "prop-types";
import Icon from "../Icon/Icon";
import "./Favorites.scss";

const Favorites = (art) => {
  const article = art.art;
  const [isFavorites, setIsFavorites] = useState(false);

  useEffect(() => {
    const fav = JSON.parse(localStorage.getItem("favorites"));
    if (!fav) {
      return;
    }
    if (fav.includes(article)) {
      setIsFavorites(true);
    }
  }, [article]);

  const FavoritesHandler = () => {
    const fav = JSON.parse(localStorage.getItem("favorites"));

    if (isFavorites) {
      let newFav = [];
      fav.forEach((f) => {
        if (f !== article) {
          newFav.push(f);
          return;
        }
      });
      localStorage.setItem("favorites", JSON.stringify([...newFav]));
      setIsFavorites(false);
      return;
    }

    if (typeof fav !== array) {
      localStorage.setItem("favorites", JSON.stringify([article]));
      setIsFavorites(true);
    }

    if (!isFavorites) {
      localStorage.setItem("favorites", JSON.stringify([...fav, article]));
      setIsFavorites(true);
      return;
    }
  };

  return (
    <div className="favorites">
      {!isFavorites && <Icon type="star" color="gold" width={25} height={26} onClick={FavoritesHandler} />}
      {isFavorites && <Icon type="star" color="gold" filled width={25} height={26} onClick={FavoritesHandler} />}
    </div>
  );
};

Favorites.propTypes = {
  art: PropTypes.string.isRequired,
};

export default Favorites;
