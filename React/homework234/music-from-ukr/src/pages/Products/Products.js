import React, { useEffect } from 'react';
import Loader from '../../components/Loader/Loader';
import ProductItem from '../../components/ProductItem/ProductItem';
import { useDispatch, useSelector } from 'react-redux';
import { getProductsSelector } from '../../store/Products/selectors';
import { getProductsOperation } from '../../store/Products/operations';
import { pageLoadingSelector } from '../../store/PageLoading/selectors';
import { getCartOperation } from '../../store/Cart/operations';

const Products = () => {
  const dispatch = useDispatch();
  const products = useSelector(getProductsSelector);
  const isLoading = useSelector(pageLoadingSelector);

  useEffect(() => {
    dispatch(getProductsOperation());
    dispatch(getCartOperation());
  }, [dispatch]);

  if (isLoading) {
    return <Loader />;
  }

  const productList = products.map(p => <ProductItem key={p.article} product={p} />);

  return <div className='product__list'>{productList}</div>;
};

export default Products;
