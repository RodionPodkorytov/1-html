import React, { PureComponent } from 'react'
import './Modal.scss'

export default class Modal extends PureComponent {

    render() {
        const {header, closeButton, text, actions, closeBtnHandler, modalRef, closeRef} = this.props

        return (
            <div onClick={closeBtnHandler} className='modal-fade'>
                <div className='modal' ref={modalRef}>
                    <div className='modal__header'>
                        <h2 className='modal__title'>{header}</h2>
                        {closeButton && <span  className='modal__close' ref={closeRef}>&times;</span>}
                    </div>
                    <div className='modal__body'><p>{text}</p></div>
                    <div className='modal__footer'>{actions}</div>
                </div>
            </div>
        )
    }
}
