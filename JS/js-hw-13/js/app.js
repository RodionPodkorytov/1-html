let styleTheme = document.querySelector('.main-theme');
let btn = document.querySelector('.change-theme');

if (localStorage.getItem('theme') === 'turquoise'){
    styleTheme.setAttribute('href', 'css/turquoise-theme.css');
}

function addThemeToLocalStorage(){
    if(localStorage.getItem('theme') === 'turquoise'){
        localStorage.setItem('theme', '');
        styleTheme.setAttribute('href', 'css/style.css');
    } else {
        localStorage.setItem('theme', 'turquoise');
        styleTheme.setAttribute('href', 'css/turquoise-theme.css');
    }
}

btn.addEventListener('click', addThemeToLocalStorage);
