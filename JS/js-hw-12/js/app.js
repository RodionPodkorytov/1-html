//setTimeout вызывает функцию только раз тогда как интервал вызывает функцию регулярно
// функция сработает через 4 милисекунды
// для того что-бы функция не захламлял стэк


const images = document.querySelector('.images-wrapper');
const buttonStop = document.createElement('button');
const buttonRestore = document.createElement('button');
const delay = 10000;

for(let i = 0; i < images.children.length; i++){
	images.children[i].dataset.index = i;
	if(i !== 0) {
		images.children[i].hidden = true;
	}
};

buttonStop.innerText = "Stop";
buttonRestore.innerText = "Restore";
buttonRestore.disabled = true;
document.body.appendChild(buttonStop);
document.body.appendChild(buttonRestore);
let intervalId;
function startInterval() {
	intervalId = setInterval(() => {
		let currentImage = images.querySelector('img:not([hidden])');
		currentImage.hidden = true;
		if (currentImage === images.lastElementChild) {
			images.firstElementChild.hidden = false;
		} else {
			images.children[+currentImage.dataset.index + 1].hidden = false;
		}
	}, delay)
};
function stopBanner() {
	clearInterval(intervalId);
	buttonStop.disabled = true;
	buttonRestore.disabled = false;
}
function restoreBanner() {
	startInterval();
	buttonStop.disabled = false;
	buttonRestore.disabled = true;
}
buttonStop.addEventListener('click', stopBanner );
buttonRestore.addEventListener('click', restoreBanner);
startInterval();
