const secondLabel = document.querySelector('label:nth-child(2)');
const inputs = document.querySelectorAll('input');
const firstInput = inputs[0];
const secondInput = inputs[1];
const btn = document.querySelector('.btn');
const icons = document.querySelectorAll('i');
const iconFirstInput = icons[0];
const iconSecondInput = icons[1];
const err = document.createElement('p');
err.innerText = 'You need to enter the same values';
err.style.color = 'red';

function showPassWord(event) {
	if (event.target.classList.contains('fa-eye-slash')) {
		event.target.classList.remove('fa-eye-slash');
		event.target.classList.add('fa-eye');
		event.target.previousElementSibling.type = 'text';
	} else {
		event.target.classList.remove('eye-slash');
		event.target.classList.add('fa-eye-slash');
		event.target.previousElementSibling.type = 'password';
	}
};

function compareValue() {
	if (firstInput.value === secondInput.value) {
		if (secondLabel.contains(err)){
			err.remove();
		}
		alert('You are welcome');
	} else {
		secondInput.parentElement.append(err);
	}
};

iconFirstInput.addEventListener('click', showPassWord);
iconSecondInput.addEventListener('click', showPassWord);
btn.addEventListener('click', compareValue);

