

	const tabs = document.body.querySelector('.tabs');
	const tabsContent = document.body.querySelector('.tabs-content');

	function handlerEventListener(event) {
		tabs.querySelector('.active').classList.remove('active');
		event.target.classList.add('active');
		tabsContent.querySelector('li:not([hidden])').hidden = true;
		tabsContent.children[event.target.dataset.index].hidden = false;
	};

	for (let i = 0; i < tabs.childElementCount; i++) {
		tabs.children[i].dataset.index = i;
		if (i) {
			tabsContent.children[i].hidden = true;
		}
	};

	tabs.addEventListener('click', handlerEventListener);
