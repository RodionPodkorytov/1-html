

	const form = document.createElement('form');
	document.body.prepend(form);
	const inputPrice = document.createElement('input');
	inputPrice.classList.add('#inputPrice');
	inputPrice.type = 'number';
	inputPrice.placeholder = 'Price';
	const label = document.createElement('label');
	label.innerText = 'Please enter price: ';
	label.htmlFor = 'inputPrice';
	form.prepend(inputPrice);
	form.prepend(label);
	const div = document.createElement('div');
	const span = document.createElement('span');
	const button = document.createElement('button');
	button.innerText = 'x';
	button.onclick = () => {
		button.parentElement.remove();
		inputPrice.value = null;
		inputPrice.style.backgroundColor = '';
	};
	const notification = document.createElement('p');

	inputPrice.onfocus = () => inputPrice.style.borderColor = 'green';

	inputPrice.onblur = () =>{
		if (+inputPrice.value >= 0) {
			inputPrice.style.borderColor = '';
		}
	};

	inputPrice.onchange = () => {
		if(+inputPrice.value >= 0){
			notification.remove();
			span.innerText = `Current price is: ${inputPrice.value}`;
			div.appendChild(span);
			div.appendChild(button);
			document.body.prepend(div);
			inputPrice.style.backgroundColor='green';
		} else {
				inputPrice.style.borderColor = 'red';
				notification.innerText = 'Please enter correct price';
				document.body.append(notification);
		}
	};


