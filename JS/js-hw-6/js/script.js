"use strict"

const filterBy = (array, typeOfData) => array.filter(elemOfArray => typeof elemOfArray !== typeOfData);
console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));
