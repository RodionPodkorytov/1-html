"use strict";

function createNewUser() {
	let firstName = prompt('Please, enter you first name');
	let lastName = prompt('Please enter your last name');
	let birthday = prompt('Please enter your birthday (dd.mm.yyyy)')
	const newUser = {
		firstName: firstName,
		lastName: lastName,
		birthday: birthday,
		getLogin: function() {
			return (this.firstName.slice(0, 1) + this.lastName).toLowerCase();
		},
		getAge: function () {
			return new Date().getFullYear() - +this.birthday.slice(-4);
		},
		getPassword: function () {
			return this.firstName[0].toLocaleUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
		},
	};
	return newUser;
};

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());
