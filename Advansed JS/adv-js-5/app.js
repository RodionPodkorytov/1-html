"use strict";

const searchBtn = document.getElementById('geoSearch');
searchBtn.addEventListener('click', ev => {
    const info = document.querySelector('.geodata');
    if(info) {
      info.remove();
      renderUserLocation('container');
    } else {
      renderUserLocation('container');
    }
  });


async function getUserIP() {
  const response = await fetch('http://api.ipify.org/?format=json');
  return (await response).json();
}

async function getUserLocation() {
  const ip = (await getUserIP()).ip;
  const userLocationResponse = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district&lang=ru`);
  const userLocation = await userLocationResponse.json();
  const {continent,country,regionName,city,district} = userLocation;
  
  const output = new Map();
          output.set('континент',    continent);
          output.set('страна',       country);
          output.set('регион',       regionName);
          output.set('город',        city);
          output.set('район города', district);

          return output;
}

async function renderUserLocation(сontainerID) {
  const container = document.getElementById(сontainerID);
  const list = document.createElement('ul');
  list.className = 'geodata';

  const geo = await getUserLocation();

  for(let key of geo.keys()) {
    const listItem = document.createElement('li');
    listItem.innerText = `${key}: ${geo.get(key)}`;
    list.append(listItem);
  }
  container.append(list);
}
