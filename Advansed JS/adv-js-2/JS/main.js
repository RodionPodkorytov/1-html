"use strict"

const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];

  const verifiedBooks = [];

  let checkObj = (books) => {
      let numObj = 1;

      for (let { author: authorValue, name: nameValue, price: priceValue, } of books) {
          try {
              let key = '';
              if (!authorValue){
                  key += ' no author';
              }
              if (!nameValue){
                  key += ' no name';
              }
              if (!priceValue){
                  key += ' no price';
              }
              if (!authorValue || !nameValue || !priceValue) {
                  throw new Error(` In object  №${numObj} this parametr is not avaliable: ${key}`)
              }

          } catch (e){
              console.log(e.message);
              continue
          } finally {
              numObj++
          }
          let book = {author: authorValue, name: nameValue, price: priceValue};
          verifiedBooks.push(book)
      };
  };
 
  let root = document.getElementById ("root");

  let addUl = function (root, verifiedBooks) {
    let ul = document.createElement('ul');
    root.append(ul)
    for (let {author, name, price} of verifiedBooks) {
      let li = document.createElement('li');
      li.innerHTML = 
      `<b>Автор книги:</b> ${author}<br><b>Название книги:</b> ${name}<br><b>Цена книги:</b> ${price}`;
      li.style.listStyle = 'none';
      li.style.fontSize = '20px';
      li.style.paddingBottom = '20px';
      ul.append(li)
    }
  }
  checkObj(books);
  addUl(root, verifiedBooks);
  