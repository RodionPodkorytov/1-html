"use strict"
class MovieCard {
    
  constructor({episode_id,title,opening_crawl,characters}) {
      this.episodeID      = episode_id;
      this.title          = title;
      this.openingCrawl   = opening_crawl;
      this.characters     = characters;

      this.elements = {
          card:           document.createElement('div'),
          episode:        document.createElement('h3'),
          title:          document.createElement('h4'),
          openingCrawl:   document.createElement('p'),
          characters:     document.createElement('ul')
      };
  }

  getCharacters() {
      const characters = this.characters;
      const characterList = this.elements.characters;
      Promise.all(characters.map(char => fetch(char)))
          .then(response => 
              Promise.all(response.map(r => r.json()))
                  .then(charData => {
                      charData.forEach(charData => {
                          let charName = document.createElement('li');
                          charName.textContent = charData.name;
                          characterList.append(charName);
                      });
                  })
      );
      return characterList;
  }

  render(containerID) {
      const container = document.getElementById(containerID);
      const {card, episode, title, openingCrawl} = this.elements;
     
      episode.textContent         = this.episodeID;
      title.textContent           = this.title;
      openingCrawl.textContent    = this.openingCrawl;       

      card.append(episode,title, this.getCharacters(), openingCrawl);
      container.append(card);
  }
}

fetch('https://swapi.dev/api/films/')
  .then(response => response.json())
  .then(data => {
      const movies = data.results.map(el => new MovieCard(el));
      
      movies.forEach(film => {
          film.render('moviewrapper');
      });

  });


