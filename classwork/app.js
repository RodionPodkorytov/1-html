'use strict'

class Person {
    constructor (firstName, lastName, group) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.group = group;
    }
    makeCoffee() {
        console.log('making coffe');
    }
    comeToWorkspace() {
        console.log('came to work space');
    }
    useWifi() {
        console.log('using wifi');
    }
}
const Rodion = new Person ('Rodion', 'Podkorytov', 'fe20');
Rodion.makeCoffee();
console.log(Rodion);